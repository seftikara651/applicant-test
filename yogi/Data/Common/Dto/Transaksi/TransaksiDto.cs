﻿using System;

namespace Common.Dto.Transaksi
{
	public class TransaksiDto
	{
		public int TransaksiId { get; set; }
		public int AccountId { get; set; }
		public DateTime TransactionDate { get; set; }
		public string Description { get; set; }
		public string DebitCreditStatus { get; set; }
		public decimal Amount { get; set; }
	}

	public class TransaksiInputDto
	{
		public int AccountId { get; set; }
		public DateTime TransactionDate { get; set; }
		public string Description { get; set; }
		public string DebitCreditStatus { get; set; }
		public decimal Amount { get; set; }
	}

	public class TransaksiUpdateDto : TransaksiInputDto
	{
		public int TransaksiId { get; set; }
	}
}
