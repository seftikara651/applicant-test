﻿using System;

namespace Common.Dto.Transaksi
{
	public class TransaksiLaporanDto
	{
		public string Description { get; set; }
		public DateTime TransactionDate { get; set; }
		public decimal Credit { get; set; }
		public decimal Debit { get; set; }
		public decimal Amount { get; set; }
	}
}
