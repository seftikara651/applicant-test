﻿namespace Common.Dto.Nasabah
{
	public class NasabahPointDto
	{
		public int AccountId { get; set; }
		public string Name { get; set; }
		public decimal TotalPoint { get; set; }
	}
}
