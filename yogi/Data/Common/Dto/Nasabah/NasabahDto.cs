﻿namespace Common.Dto.Nasabah
{
	public class NasabahDto
	{
		public int AccountId { get; set; }
		public string Name { get; set; }
	}

	public class NasabahInputDto : NasabahDto
	{
	}

	public class NasabahUpdateDto : NasabahDto
	{
	}
}
