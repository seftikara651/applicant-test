﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Model
{
	public class ModelTransaksi
	{
		[Key]
		public int TransaksiId { get; set; }
		public DateTime TransactionDate { get; set; }
		public string Description { get; set; }
		[StringLength(1)]
		public string DebitCreditStatus { get; set; }
		public decimal Amount { get; set; }

		public int AccountId { get; set; }
		[ForeignKey("AccountId")]
		public ModelNasabah Nasabah { get; set; }
	}
}
