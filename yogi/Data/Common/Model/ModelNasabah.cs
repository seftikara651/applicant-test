﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Common.Model
{
	public class ModelNasabah
	{
		[Key]
		public int AccountId { get; set; }
		public string Name { get; set; }
		public virtual ICollection<ModelTransaksi> Transaksi { get; set; }
	}
}
