﻿using System.Collections.Generic;

namespace Common.Base
{
	public class ServiceResult
    {
        public ServiceResult()
        {
            BadRequest();
        }
        public int Code { get; set; }
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }

        public void SetMessage(string message)
        {
            Message = message;
        }
        public void OK()
        {
            Code = 200;
            Succeeded = true;
            Message = "All OK";
            Description = "";
        }
        public void OK(string message)
        {
            OK();
            Message = message;
        }
        public void BadRequest()
        {
            Code = 400;
            Succeeded = false;
            Message = "Bad Request";
            Description = "";
        }
        public void BadRequest(string message)
        {
            BadRequest();
            Message = message;
            Description = "";
        }
        public void UnAuthorized()
        {
            Code = 401;
            Succeeded = false;
            Message = "Unauthorized";
            Description = "";
        }
        public void Confirmation()
        {
            Code = 205;
            Succeeded = false;
            Message = "Confirmation";
            Description = "";
        }
        public void Confirmation(string message)
        {
            Confirmation();
            Message = message;
            Description = "";
        }
        public void MultipleChoice()
        {
            Code = 300;
            Succeeded = false;
            Message = "Multiple Choice";
            Description = "";
        }
        public void MultipleChoice(string message)
        {
            Confirmation();
            Message = message;
            Description = "";
        }
        public void UnAuthorized(string message)
        {
            UnAuthorized();
            Message = message;
            Description = "";
        }
        public void NotFound()
        {
            Code = 404;
            Succeeded = false;
            Message = "Not Found";
        }
        public void NotFound(string message)
        {
            NotFound();
            Message = message;
            Description = "";
        }
        public void Error(string message, string description)
        {
            Code = 500;
            Succeeded = false;
            Message = message;
            Description = description;
        }

        public void Forbidden(string message)
        {
            Code = 403;
            Succeeded = false;
            Message = message;
            Description = "";
        }
    }
    public class APIResult : ServiceResult
    {
        public string Response { get; set; }
    }
    public class ObjectResult<T>
    {
        public ObjectResult()
        {
            Status = new ServiceResult();
        }
        public T Obj { get; set; }
        public ServiceResult Status { get; set; }

        public void NotFound()
        {
            Status.NotFound();
        }

        public void Error(string message, string description)
        {
            Status.Error(message, description);
        }
    }
    public class ListResult<T>
    {
        public ListResult()
        {
            ListObj = new List<T>();
            Status = new ServiceResult();
        }
        public List<T> ListObj { get; set; }
        public ServiceResult Status { get; set; }
    }

    public class ListPageResult<T> : ListResult<T>
    {
        public int Count { get; set; }
    }
}
