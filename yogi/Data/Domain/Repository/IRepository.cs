﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Repository
{
	public interface IRepository<TDbContext> : IDisposable
    {
        bool HasActiveTransaction { get; }
        IQueryable<TEntity> Entity<TEntity>() where TEntity : class;
        Task<List<TEntity>> GetListAsync<TEntity>(IQueryable<TEntity> query);
        Task<TEntity> GetDataAsync<TEntity>(IQueryable<TEntity> query);
        Task<int> GetCountAsync<TEntity>(IQueryable<TEntity> query);
        void Add<TEntity>(ref TEntity entity) where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;
        void Remove<TEntity>(TEntity entity) where TEntity : class;
        Task<ServiceResult> SaveChangesAsync();
        void BeginTransaction();
        Task CommitAsync();
        Task RollbackAsync();
        Task<ServiceResult> ExecuteCommand(string query);
        ObjectResult<T> GetDataCommand<T>(string query);
        ListResult<T> GetListCommand<T>(string query);
    }
}
