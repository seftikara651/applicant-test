﻿using Common.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
	public interface IUnitOfWork<TDbContext>
    {
        IQueryable<TEntity> Entity<TEntity>() where TEntity : class;
        Task<List<TEntity>> GetListAsync<TEntity>(IQueryable<TEntity> query);
        Task<TEntity> GetDataAsync<TEntity>(IQueryable<TEntity> query);
        Task<int> GetCountAsync<TEntity>(IQueryable<TEntity> query);
        Task<ObjectResult<TEntity>> AddAsync<TEntity>(TEntity entity) where TEntity : class;
        Task<ServiceResult> UpdateAsync<TEntity>(TEntity entity) where TEntity : class;
        Task<ServiceResult> RemoveAsync<TEntity>(TEntity entity) where TEntity : class;
        void BeginTransaction();
        Task<ServiceResult> CommitAsync();
        Task<ServiceResult> ExecuteCommandAsync(string query, bool withLog = true);
        ObjectResult<T> GetDataCommand<T>(string query, bool withLog = true);
        ListResult<T> GetListCommand<T>(string query, bool withLog = true);
    }
}
