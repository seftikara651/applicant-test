﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Repository
{
	public class UnitOfWork<TDbContext> : IUnitOfWork<TDbContext>
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IRepository<TDbContext> _repository;
        public UnitOfWork(IRepository<TDbContext> repository)
        {
            _repository = repository;
        }
        public IQueryable<TEntity> Entity<TEntity>() where TEntity : class//, IEntity<TDbContext>
        {
            return _repository.Entity<TEntity>();
        }
        public async Task<List<TEntity>> GetListAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await _repository.GetListAsync(query);
        }
        public async Task<TEntity> GetDataAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await _repository.GetDataAsync(query);
        }
        public async Task<int> GetCountAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await _repository.GetCountAsync(query);
        }
        public async Task<ObjectResult<TEntity>> AddAsync<TEntity>(TEntity entity) where TEntity : class//, IEntity<TDbContext>
        {
            ObjectResult<TEntity> result = new ObjectResult<TEntity>();
            try
            {
                _repository.Add(ref entity);
                if (!_repository.HasActiveTransaction)
                    result.Status = await _repository.SaveChangesAsync();

                result.Obj = entity;
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
                result.Status.Error("Failed add", ex.Message);
            }
            return result;
        }
        public async Task<ServiceResult> UpdateAsync<TEntity>(TEntity entity) where TEntity : class//, IEntity<TDbContext>
        {
            ServiceResult result = new ServiceResult();
            try
            {
                _repository.Update(entity);
                if (!_repository.HasActiveTransaction)
                    result = await _repository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
                result.Error("Failed update", ex.Message);
            }
            return result;
        }
        public async Task<ServiceResult> RemoveAsync<TEntity>(TEntity entity) where TEntity : class//, IEntity<TDbContext>
        {
            ServiceResult result = new ServiceResult();
            try
            {
                _repository.Remove(entity);
                if (!_repository.HasActiveTransaction)
                    result = await _repository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
                result.Error("Failed remove", ex.Message);
            }
            return result;
        }
        public void BeginTransaction()
        {
            if (!_repository.HasActiveTransaction)
                _repository.BeginTransaction();
        }
        public async Task<ServiceResult> CommitAsync()
        {
            ServiceResult result = new ServiceResult();
            try
            {
                await _repository.CommitAsync();
                result.OK();
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
                try
                {
                    await _repository.RollbackAsync();
                    result.Error("Commit failed, any changes is rolled back", ex.Message);
                }
                catch
                {
                    result.Error("Commit failed, any changes is failed to rolled back", ex.Message);
                }

            }
            return result;
        }

        public Task<ServiceResult> ExecuteCommandAsync(string query, bool withLog = true)
        {
            if (withLog)
                log.Info(query);

            return _repository.ExecuteCommand(query);
        }

        public ObjectResult<T> GetDataCommand<T>(string query, bool withLog = true)
        {
            if (withLog)
                log.Info(query);

            return _repository.GetDataCommand<T>(query);
        }

        public ListResult<T> GetListCommand<T>(string query, bool withLog = true)
        {
            if (withLog)
                log.Info(query);

            return _repository.GetListCommand<T>(query);
        }
    }
}
