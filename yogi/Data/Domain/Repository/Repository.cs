﻿using Common.Base;
using Dapper;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Repository
{
	public class Repository : IRepository<PadepokanContext>
	{
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly PadepokanContext _context;
        private IDbContextTransaction _transaction;

        public Repository(PadepokanContext context)
        {
            _context = context;
        }

        public bool HasActiveTransaction
        {
            get
            {
                if (_transaction == null)
                    return false;
                else
                    return true;
            }
        }

        public IQueryable<TEntity> Entity<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>();
        }

        public async Task<int> GetCountAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await query.CountAsync();
        }

        public async Task<TEntity> GetDataAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await query.FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetListAsync<TEntity>(IQueryable<TEntity> query)
        {
            return await query.ToListAsync();
        }

        public void Add<TEntity>(ref TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Add(entity);
            _context.Entry(entity).State = EntityState.Added;
        }

        public void AddRange<TEntity>(ref List<TEntity> entity) where TEntity : class
        {
            _context.Set<TEntity>().AddRange(entity);
            _context.Entry(entity).State = EntityState.Added;
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Deleted;
        }
        public void RemoveRange<TEntity>(List<TEntity> entity) where TEntity : class//, IEntity<aktuariaContext>
        {
            _context.Set<TEntity>().AttachRange(entity);
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public async Task<ServiceResult> SaveChangesAsync()
        {
            ServiceResult result = new ServiceResult();
            try
            {
                await _context.SaveChangesAsync();
                result.OK();
            }
            catch (Exception ex)
            {
                result.Error("Error in Service Database", ex.Message);
            }
            return result;
        }

        public void BeginTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        public async Task CommitAsync()
        {
            await SaveChangesAsync();
            await Task.Run(() =>
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            });
        }

        public async Task RollbackAsync()
        {
            await Task.Run(() =>
            {
                _transaction.Rollback();
                _transaction.Dispose();
                _transaction = null;
            });
        }

        public async Task<ServiceResult> ExecuteCommand(string query)
        {
            ServiceResult result = new ServiceResult();
            try
            {
                var count = await _context.Database.ExecuteSqlRawAsync(query);
                await _context.SaveChangesAsync();
                result.OK();
            }
            catch (Exception ex)
            {
                result.Error("Error Service In Database", ex.Message);
            }
            return result;
        }

        public void Dispose()
        {
            Thread.Sleep(100);
            _context.Dispose();
        }

        public ObjectResult<T> GetDataCommand<T>(string query)
        {
            ObjectResult<T> result = new ObjectResult<T>();
            try
            {
                IDbConnection db = _context.Database.GetDbConnection();
                if (db == null)
                {
                    _context.Database.OpenConnection();
                    db = _context.Database.GetDbConnection();
                }

                result.Obj = db.Query<T>(query, null, commandType: CommandType.Text).FirstOrDefault();

                result.Status.OK();
            }
            catch (Exception ex)
            {
                result.Status.Error("Error Service In Database", ex.Message);
            }
            return result;
        }

        public ListResult<T> GetListCommand<T>(string query)
        {
            ListResult<T> result = new ListResult<T>();
            try
            {
                IDbConnection db = _context.Database.GetDbConnection();
                if (db == null)
                {
                    _context.Database.OpenConnection();
                    db = _context.Database.GetDbConnection();

                }

                result.ListObj = db.Query<T>(query, null, commandType: CommandType.Text).ToList();
                result.Status.OK();
            }
            catch (Exception ex)
            {
                result.Status.Error("Error Service In Database", ex.Message);
            }
            return result;
        }
    }
}
