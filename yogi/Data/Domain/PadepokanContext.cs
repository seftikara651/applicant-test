﻿using Common.Model;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
	public class PadepokanContext : DbContext
	{
        public PadepokanContext()
        {
        }

        public PadepokanContext(DbContextOptions<PadepokanContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ModelNasabah> Nasabah { get; set; }
        public virtual DbSet<ModelTransaksi> Transaksi { get; set; }
    }
}
