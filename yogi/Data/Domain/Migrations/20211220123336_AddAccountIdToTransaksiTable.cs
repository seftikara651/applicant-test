﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class AddAccountIdToTransaksiTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountId",
                table: "Transaksi",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Transaksi_AccountId",
                table: "Transaksi",
                column: "AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transaksi_Nasabah_AccountId",
                table: "Transaksi",
                column: "AccountId",
                principalTable: "Nasabah",
                principalColumn: "AccountId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transaksi_Nasabah_AccountId",
                table: "Transaksi");

            migrationBuilder.DropIndex(
                name: "IX_Transaksi_AccountId",
                table: "Transaksi");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Transaksi");
        }
    }
}
