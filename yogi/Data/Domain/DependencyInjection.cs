﻿using Domain.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Domain
{
	public static class DependencyInjection
	{
        /// <summary>
        /// Add generic repository services to the .NET Dependency Injection container.
        /// </summary>
        /// <param name="services">The type to be extended.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="services"/> is <see langword="null"/>.</exception>

        public static void AddRepository(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddTransient<IRepository<PadepokanContext>, Repository.Repository>();
            services.AddTransient(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
        }
    }
}
