﻿using AutoMapper;
using Common.Base;
using Common.Dto.Nasabah;
using Common.Model;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NasabahService.Service
{
	public class NasabahService : INasabahService
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private readonly IUnitOfWork<PadepokanContext> _context;
		private readonly IMapper _mapper;

		public NasabahService(IUnitOfWork<PadepokanContext> context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<ObjectResult<NasabahDto>> CreateNasabahAsync(NasabahInputDto input)
		{
			var result = new ObjectResult<NasabahDto>();
			try
			{
				if (_context.Entity<ModelNasabah>().Any(x => x.AccountId == input.AccountId))
				{
					result.Status.BadRequest($"Nasabah dengan Account Id = {input.AccountId} sudah terekam.");
					return result;
				}

				var createNasabah = await _context.AddAsync(_mapper.Map<ModelNasabah>(input));
				result.Status = createNasabah.Status;
				if (!createNasabah.Status.Succeeded)
				{
					return result;
				}
				result.Obj = _mapper.Map<NasabahDto>(createNasabah.Obj);
			}
			catch (Exception ex)
			{
				log.Error("Failed Create Nasabah", ex);
				result.Error("Failed Create Nasabah", ex.Message);
			}

			return result;
		}

		public async Task<ServiceResult> DeleteNasabahAsync(int accountId)
		{
			var result = new ServiceResult();
			try
			{
				var nasabah = _context.Entity<ModelNasabah>().FirstOrDefault(x => x.AccountId == accountId);
				if (nasabah == null)
				{
					result.OK();
					return result;
				}
				result = await _context.RemoveAsync(nasabah);
			}
			catch (Exception ex)
			{
				log.Error("Failed Delete Nasabah", ex);
				result.Error("Failed Delete Nasabah", ex.Message);
			}

			return result;
		}

		public async Task<ListResult<NasabahDto>> GetListNasabahAsync()
		{
			var result = new ListResult<NasabahDto>();
			try
			{
				result.ListObj = _mapper.Map<List<NasabahDto>>(await _context.GetListAsync(_context.Entity<ModelNasabah>()));
				result.Status.OK();
			}
			catch (Exception ex)
			{
				log.Error("Failed Get List Nasabah", ex);
				result.Status.Error("Failed Get List Nasabah", ex.Message);
			}

			return result;
		}

		public ListResult<NasabahPointDto> GetListPointNasabah()
		{
			var result = new ListResult<NasabahPointDto>();
			try
			{
				result.ListObj = _context.GetListCommand<NasabahPointDto>(@"with result_points as (select b.AccountId, b.Name, a.Amount, 
				(case 
				when a.Amount <= 10000 then 0
				when a.Amount > 10000 and a.Amount <= 30000 and UPPER(a.Description = 'BELI PULSA') then
				(a.Amount / 1000) * 1
				when a.Amount > 30000 and UPPER(a.Description = 'BELI PULSA') then
				(a.Amount / 1000) * 2
				when a.Amount > 50000 and a.Amount <= 100000 and UPPER(a.Description = 'BELI LISTRIK') then
				(a.Amount / 2000) * 1
				when a.Amount > 100000 and UPPER(a.Description = 'BELI LISTRIK') then
				(a.Amount / 2000) * 2
				else 0
				end) as Points from transaksi a,
				nasabah b where a.AccountId = b.AccountId)
				select AccountId, Name, SUM(Points) as TotalPoint from result_points group by AccountId, Name;").ListObj;

				result.Status.OK();
			}
			catch (Exception ex)
			{
				log.Error("Failed Get List Nasabah Point", ex);
				result.Status.Error("Failed Get List Nasabah Point", ex.Message);
			}

			return result;
		}

		public async Task<ObjectResult<NasabahDto>> UpdateNasabahAsync(NasabahUpdateDto update)
		{
			var result = new ObjectResult<NasabahDto>();
			try
			{
				var nasabah = _context.Entity<ModelNasabah>().FirstOrDefault(x => x.AccountId == update.AccountId);
				if (nasabah == null)
				{
					result.Status.BadRequest($"Nasabah dengan Account Id = {update.AccountId} tidak ditemukan.");
					return result;
				}

				nasabah.Name = update.Name;
				var updateNasabah = await _context.UpdateAsync(nasabah);
				result.Status = updateNasabah;
				if (!updateNasabah.Succeeded)
				{
					return result;
				}
				result.Obj = _mapper.Map<NasabahDto>(nasabah);
			}
			catch (Exception ex)
			{
				log.Error("Failed Update Nasabah", ex);
				result.Error("Failed Update Nasabah", ex.Message);
			}

			return result;
		}
	}
}
