﻿using Common.Base;
using Common.Dto.Nasabah;
using System.Threading.Tasks;

namespace NasabahService.Service
{
	public interface INasabahService
	{
		Task<ListResult<NasabahDto>> GetListNasabahAsync();
		Task<ObjectResult<NasabahDto>> CreateNasabahAsync(NasabahInputDto input);
		Task<ObjectResult<NasabahDto>> UpdateNasabahAsync(NasabahUpdateDto input);
		Task<ServiceResult> DeleteNasabahAsync(int accountId);
		ListResult<NasabahPointDto> GetListPointNasabah();
	}
}
