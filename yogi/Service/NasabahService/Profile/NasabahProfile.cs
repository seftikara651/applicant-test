﻿using Common.Dto.Nasabah;
using Common.Model;

namespace NasabahService.Profile
{
	public class NasabahProfile : AutoMapper.Profile
	{
		public NasabahProfile()
		{
			CreateMap<NasabahInputDto, ModelNasabah>();
			CreateMap<ModelNasabah, NasabahDto>();
		}
	}
}
