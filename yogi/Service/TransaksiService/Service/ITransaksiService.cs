﻿using Common.Base;
using Common.Dto.Transaksi;
using System;
using System.Threading.Tasks;

namespace TransaksiService.Service
{
	public interface ITransaksiService
	{
		Task<ListResult<TransaksiDto>> GetListTransaksiAsync();
		Task<ObjectResult<TransaksiDto>> CreateTransaksiAsync(TransaksiInputDto input);
		ListResult<TransaksiLaporanDto> GetListTransaksiLaporan(int accountId, DateTime startDate, DateTime endDate);
	}
}
