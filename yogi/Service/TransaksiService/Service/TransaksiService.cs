﻿using AutoMapper;
using Common.Base;
using Common.Dto.Transaksi;
using Common.Model;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransaksiService.Service
{
	public class TransaksiService : ITransaksiService
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private readonly IUnitOfWork<PadepokanContext> _context;
		private readonly IMapper _mapper;

		public TransaksiService(IUnitOfWork<PadepokanContext> context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<ObjectResult<TransaksiDto>> CreateTransaksiAsync(TransaksiInputDto input)
		{
			var result = new ObjectResult<TransaksiDto>();
			try
			{
				if (input.DebitCreditStatus?.ToUpper() != "D" && input.DebitCreditStatus?.ToUpper() != "C")
				{
					result.Status.BadRequest("Input DebitCreditStatus dengan Huruf D = Debit atau C = Credit");
					return result;
				}

				if (!_context.Entity<ModelNasabah>().Any(x => x.AccountId == input.AccountId))
				{
					result.Status.NotFound($"Account Id {input.AccountId} tidak ditemukan");
					return result;
				}

				var createTransaksi = await _context.AddAsync(_mapper.Map<ModelTransaksi>(input));
				result.Status = createTransaksi.Status;
				if (!createTransaksi.Status.Succeeded)
				{
					return result;
				}
				result.Obj = _mapper.Map<TransaksiDto>(createTransaksi.Obj);
			}
			catch (Exception ex)
			{
				log.Error("Failed Create Transaksi", ex);
				result.Error("Failed Create Transaksi", ex.Message);
			}

			return result;
		}

		public async Task<ListResult<TransaksiDto>> GetListTransaksiAsync()
		{
			var result = new ListResult<TransaksiDto>();
			try
			{
				result.ListObj = _mapper.Map<List<TransaksiDto>>(await _context.GetListAsync(_context.Entity<ModelTransaksi>()));
				result.Status.OK();
			}
			catch (Exception ex)
			{
				log.Error("Failed Get List Transaksi", ex);
				result.Status.Error("Failed Get List Transaksi", ex.Message);
			}

			return result;
		}

		public ListResult<TransaksiLaporanDto> GetListTransaksiLaporan(int accountId, DateTime startDate, DateTime endDate)
		{
			var result = new ListResult<TransaksiLaporanDto>();
			try
			{
				if (startDate.Date > endDate.Date)
				{
					result.Status.BadRequest("Start Date harus lebih kecil dari End Date");
					return result;
				}

				result.ListObj = _context.GetListCommand<TransaksiLaporanDto>(@$"select a.TransactionDate, a.Description, 
				(select sum(b.amount)
				from transaksi b where a.AccountId = b.AccountId
				and b.Description = a.Description
				and cast(b.TransactionDate as date) = cast(a.TransactionDate as date)
				and b.DebitCreditStatus = 'C')
				as Credit,
				(select sum(b.amount)
				from transaksi b where a.AccountId = b.AccountId 
				and b.Description = a.Description
				and cast(b.TransactionDate as date) = cast(a.TransactionDate as date)
				and b.DebitCreditStatus = 'D')
				as Debit,
				(select sum(b.amount)
				from transaksi b where a.AccountId = b.AccountId 
				and b.Description = a.Description
				and cast(b.TransactionDate as date) = cast(a.TransactionDate as date))
				as Amount
				from transaksi a where a.AccountId = {accountId} and cast(a.TransactionDate as date) between '{startDate:yyyy-MM-dd}' and '{endDate:yyyy-MM-dd}'
				group by a.Description;").ListObj;
				result.Status.OK();
			}
			catch (Exception ex)
			{
				log.Error("Failed Get List Transaksi", ex);
				result.Status.Error("Failed Get List Transaksi", ex.Message);
			}

			return result;
		}
	}
}
