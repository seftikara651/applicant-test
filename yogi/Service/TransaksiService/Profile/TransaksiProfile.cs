﻿using Common.Dto.Transaksi;
using Common.Model;

namespace TransaksiService.Profile
{
	public class TransaksiProfile : AutoMapper.Profile
	{
		public TransaksiProfile()
		{
			CreateMap<TransaksiInputDto, ModelTransaksi>();
			CreateMap<ModelTransaksi, TransaksiDto>();
		}
	}
}
