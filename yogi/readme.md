# TEST BACKEND PADEPOKAN TUJUH SEMBILAN
## First Setup

Requirement :
- .Net Core 5 (.NET 5) SDK
- MySQL
- Apache / Nginx / IIS

1.  Clone Repository
    ```
    git clone https://github.com/yogichandras/test-backend-padepokan-tujuh-sembilan.git
    ```
2.  Change ConnectionString
    ```
    in Presentation/WebService/appsettings.json
    ```
3.  Change MySQL Version
    ```
    in Presentation/WebService/Startup.cs Line 27
    ```
4.  Migration using Package Manager Console
    ```
    1. Set Startup Project to WebService
    2. Set Default Project to Data
    3. Update-Database
    ```
5. Migration using EF Core Tools
   ```
   1. cd Data/Domain
   2. dotnet ef database update
   ```
6. RUN APP
   ```
   1. View/Run with Visual Studio or
   2. cd /Presentation/WebService dotnet run
   ```