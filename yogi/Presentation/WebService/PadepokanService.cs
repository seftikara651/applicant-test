﻿using NasabahService.Service;
using System;
using Microsoft.Extensions.DependencyInjection;
using TransaksiService.Service;

namespace WebService
{
	public class PadepokanService
	{
		private IServiceProvider _scope;
		public PadepokanService(IServiceProvider scope)
		{
			_scope = scope;
		}

		public INasabahService NasabahService
		{
			get { return _scope.GetService<INasabahService>(); }
		}

		public ITransaksiService TransaksiService
		{
			get { return _scope.GetService<ITransaksiService>(); }
		}
	}
}
