﻿using Domain;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;

namespace WebService
{
	public static class DependencyInjection
	{
        public static void Register(this IServiceCollection builder)
        {
            builder.AddRepository();

            var serviceNamespaces = (new[]{
                "Common",
                "NasabahService",
                "TransaksiService",
            });

            var refAssembyNames = Assembly.GetExecutingAssembly()
                .GetReferencedAssemblies();
            foreach (var asslembyNames in refAssembyNames)
            {
                var assembly = Assembly.Load(asslembyNames);
                builder.AddAutoMapper(assembly);
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(d => serviceNamespaces.Any(y => d.FullName.StartsWith(y))).SelectMany(d => d.DefinedTypes);

            var interfaces = assemblies.Where(d => d.IsInterface).ToList();
            foreach (var @interface in interfaces)
            {
                var @class = assemblies.Where(x => @interface.IsAssignableFrom(x) && !x.IsInterface)
                                       .OrderByDescending(x => x.Name).FirstOrDefault();
                if (@class != null)
                {
                    builder.AddTransient(@interface, @class);
                }
            }
        }
    }
}
