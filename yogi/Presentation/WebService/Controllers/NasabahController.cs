﻿using Common.Base;
using Common.Dto.Nasabah;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebService.Controllers
{
	[Route("api/nasabah")]
	[ApiController]
	public class NasabahController : ControllerBase
	{
		private readonly PadepokanService _service;

		public NasabahController(PadepokanService service)
		{
			this._service = service;
		}

		[Route("create")]
		[HttpPost]
		public async Task<ObjectResult<NasabahDto>> Create(NasabahInputDto input)
		{
			return await _service.NasabahService.CreateNasabahAsync(input);
		}

		[Route("list")]
		[HttpGet]
		public async Task<ListResult<NasabahDto>> List()
		{
			return await _service.NasabahService.GetListNasabahAsync();
		}

		[Route("list_points")]
		[HttpGet]
		public ListResult<NasabahPointDto> ListPoints()
		{
			return _service.NasabahService.GetListPointNasabah();
		}

		[Route("update")]
		[HttpPut]
		public async Task<ObjectResult<NasabahDto>> Update(NasabahUpdateDto update)
		{
			return await _service.NasabahService.UpdateNasabahAsync(update);
		}

		[Route("delete")]
		[HttpDelete]
		public async Task<ServiceResult> Delete(int accountId)
		{
			return await _service.NasabahService.DeleteNasabahAsync(accountId);
		}
	}
}
