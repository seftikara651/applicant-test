﻿using Common.Base;
using Common.Dto.Transaksi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebService.Controllers
{
	[Route("api/transaksi")]
	[ApiController]
	public class TransaksiController : ControllerBase
	{
		private readonly PadepokanService _service;

		public TransaksiController(PadepokanService service)
		{
			this._service = service;
		}

		[Route("create")]
		[HttpPost]
		public async Task<ObjectResult<TransaksiDto>> Create(TransaksiInputDto input)
		{
			return await _service.TransaksiService.CreateTransaksiAsync(input);
		}

		[Route("list")]
		[HttpGet]
		public async Task<ListResult<TransaksiDto>> List()
		{
			return await _service.TransaksiService.GetListTransaksiAsync();
		}

		[Route("laporan_transaksi")]
		[HttpGet]
		public ListResult<TransaksiLaporanDto> LaporanTransaksi(int accountId, DateTime startDate, DateTime endDate)
		{
			return _service.TransaksiService.GetListTransaksiLaporan(accountId, startDate, endDate);
		}
	}
}
